-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: Questionnaire
-- ------------------------------------------------------
-- Server version	5.7.20-0ubuntu0.17.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2018_04_28_220653_create_questionnaires_table',1),('2018_04_30_004400_create_questions_table',1),('2018_04_30_004514_create_roles_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questionnaires`
--

DROP TABLE IF EXISTS `questionnaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questionnaires` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questions` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `answers` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questionnaires`
--

LOCK TABLES `questionnaires` WRITE;
/*!40000 ALTER TABLE `questionnaires` DISABLE KEYS */;
INSERT INTO `questionnaires` VALUES (1,'How likely are you to answer a multiple choice questionnaire?',0);
/*!40000 ALTER TABLE `questionnaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `questions`
--

DROP TABLE IF EXISTS `questions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `questions_question_unique` (`question`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `questions`
--

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;
/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'deanna','XXXX@example.com','$2y$10$zTaOEnUva4ffcJxReUeXqe72wmqasN/t34Sv/wwxM.X.PI1KtVTXO','ikHjLdXD6x',NULL,NULL),(2,'Rosamond Goyette','morton.stokes@example.org','$2y$10$j6rVY1SI8FVWJhve2Z4NquBHLc/jJA6QCSiFMav28.4bxlHigUWT.','sW09zAlq8Y','2018-05-02 20:44:51','2018-05-02 20:44:51'),(3,'Dena Waters','solon.kunze@example.net','$2y$10$Fx838xbB.YY1gH8nee9EEuS0jSAtT9kPosAoMzg688nIo1IyIsD0S','AkYRU018yP','2018-05-02 20:44:51','2018-05-02 20:44:51'),(4,'Mr. Aaron Graham','tkuhic@example.com','$2y$10$5dYeLPYNUdR7DpO7txfTH.O9DwRAs0vITLoVIxHUyPBVzYgjqSpDG','IIdsqJbsMT','2018-05-02 20:44:51','2018-05-02 20:44:51'),(5,'Adan Gislason','zetta08@example.com','$2y$10$F/AmrlNekRLdYYoqndUXyewsPLT89BHE8gQ1C2HwD4BDPfgXiHFoq','eAV1iklYrn','2018-05-02 20:44:51','2018-05-02 20:44:51'),(6,'Arden Rempel','mhuels@example.net','$2y$10$5xjGWVaPHuEn4mCONicSj.V6aiKxAd3UpvmC73X8ekc7YSFmc6QD6','worud8bFz0','2018-05-02 20:44:51','2018-05-02 20:44:51'),(7,'Dolores Ankunding IV','reichel.meta@example.org','$2y$10$wIb5EXHsTVPKyvekSP2bNOuo1mXsgFsZKpMy7vTzuWBYsldg0dy1O','xMoO6YFuRA','2018-05-02 20:44:51','2018-05-02 20:44:51'),(8,'Dylan Pfeffer','cwisoky@example.com','$2y$10$ChB6me4jFdTG/woktddpluJn2BSO/C3uLdBzYSa8Wonh9cPQW/MtG','iHwhfnXydp','2018-05-02 20:44:51','2018-05-02 20:44:51'),(9,'Dr. Wellington Weissnat','conn.krystel@example.net','$2y$10$mMGQ8cMnOal9bzPg4dLQ8unOOie2c7xWatG291Y3dliZErCVfWMym','Q8OPvqZu7x','2018-05-02 20:44:51','2018-05-02 20:44:51'),(10,'Aditya Jacobi','vincent.hoeger@example.com','$2y$10$4LC3j7Tlt03jqcjOTy7HvOv5FlrRo9zjl57MXRCnuvV/xUPOjEQEK','7vSGNPhnSZ','2018-05-02 20:44:51','2018-05-02 20:44:51'),(11,'Grant Runte','boyer.nikita@example.org','$2y$10$3xPXjv77IaT3lx8lYTx87.uHp7jkajofRarFZ1oaPQr6NSLiEDlv6','2wwhVzFVbi','2018-05-02 20:44:51','2018-05-02 20:44:51'),(12,'Briana Zboncak','wallace.wiegand@example.org','$2y$10$y5BeB9657OrT9dRBk4Bo1.vJ8qaPhglquMMXtIm66DIK7oJu4E2CS','6N7y8guDCg','2018-05-02 20:44:51','2018-05-02 20:44:51'),(13,'Ms. Elyssa Schowalter','dtillman@example.com','$2y$10$CIXhe5bZLfdJ.SboDxDbGemHNE56uOK60WHidKbncJe3u.X1Afq8W','MGX4SfdJCQ','2018-05-02 20:44:51','2018-05-02 20:44:51'),(14,'Loy Gibson MD','vdaugherty@example.net','$2y$10$ARADW/UB1q5am5rlkHAngeucuGR0wPvSrniooDoqM2G92biR9LJDi','ifCiVgEoum','2018-05-02 20:44:51','2018-05-02 20:44:51'),(15,'Silas Jacobi','theron.vonrueden@example.com','$2y$10$cL73NG/kPc/F0WkqvbzBHO34eoGmBE4f0rX8gT6nl70f5A9IjetG6','sNUD3Dm7iB','2018-05-02 20:44:51','2018-05-02 20:44:51'),(16,'Mr. Delmer Koepp Sr.','wswaniawski@example.com','$2y$10$OopE3P9anazjUuCUsIF7v.3RKxJgjejNXMOxyi4Ptv2.4ckUToH.2','ekPyPZfmry','2018-05-02 20:44:51','2018-05-02 20:44:51'),(17,'Sadye West','opal87@example.com','$2y$10$a0O.a6KopdK9mW0VgOfuxe0ByoaRymFy00NjLtIukW96DQ.aYZ26C','HWGPiv3zZS','2018-05-02 20:44:51','2018-05-02 20:44:51'),(18,'Mrs. Marion Trantow','moconnell@example.com','$2y$10$31rlfB03.KqXSVfSSEtRjeoUoROnI/ATcWL5kYLptuc3.efIfYbXO','KDR2KlqFzm','2018-05-02 20:44:51','2018-05-02 20:44:51'),(19,'Kieran Aufderhar','aquitzon@example.org','$2y$10$d7jv30PLS8IAmb.m.3At7u9HsYBYZpcUJ2S/DbvKY.HA/dr4UmkHO','AMQqOvoAdS','2018-05-02 20:44:51','2018-05-02 20:44:51'),(20,'Roman Glover','hackett.domenic@example.org','$2y$10$5u14GujEjlSNFoIUOi.9AeUuRaBTply0iUj2H6qSvsbuBHjOkOPSC','cucXlWlS0W','2018-05-02 20:44:51','2018-05-02 20:44:51'),(21,'Mrs. Cecilia Russel III','jwalsh@example.net','$2y$10$mBE1ZKZRKomV03jvkQYVve7Mm/5MCdOqg1bFJXghvN9Ud2dsYATXu','Ft7deCaTB5','2018-05-02 20:44:51','2018-05-02 20:44:51'),(22,'Alberto Olson','jacinto.mante@example.org','$2y$10$GWqBCa436GjlJf1VN8USeOTiFXv4krVx6p7SHBTlC.Mevdubk/qX.','0c6MMT2llq','2018-05-02 20:44:51','2018-05-02 20:44:51'),(23,'Mike Blanda','shemar19@example.com','$2y$10$zhJSicZaytPklSSPOxJAA.bhWD5ZqPrso3scEn0kb60t/B00mxCke','vSSJ7aHq3i','2018-05-02 20:44:51','2018-05-02 20:44:51'),(24,'Mr. Daryl Maggio V','vanessa67@example.com','$2y$10$BO6qRBJnZp492wKu5LY8Ju9jSc8uleaSOanROLkEE7RChwvg.d4Xy','fDH47CZTOP','2018-05-02 20:44:51','2018-05-02 20:44:51'),(25,'Prof. Mckenna Beier','harmony.spinka@example.net','$2y$10$WvAVrfqchl9CRyIultWz2uhBDB9clxijq.Gmdol3OX/QCjB2OHZOO','cmdKOJrkb0','2018-05-02 20:44:51','2018-05-02 20:44:51'),(26,'Gerhard Runolfsson','nfeeney@example.org','$2y$10$KRsUOqoMYh6Ac8vnhVMA7.BKqwDjI.yJr973DKz68HJrcnwzAckLC','OsH7qVCJOL','2018-05-02 20:44:51','2018-05-02 20:44:51'),(27,'Macey Fay MD','ulises64@example.com','$2y$10$4.7ic.lD2/FyJ5n4C3HNaOCRl5MPvtOerA/EBttWo9BIz1hqi0U..','E6ITsQmR0W','2018-05-02 20:44:51','2018-05-02 20:44:51'),(28,'Friedrich Hessel Jr.','breitenberg.dejah@example.org','$2y$10$au9ZmMo190h3mbp451PSU.6hdZTKMABh2ch.keuLcsPVpdazBH3r.','Ik33sTtPNl','2018-05-02 20:44:51','2018-05-02 20:44:51'),(29,'Chanel Davis','laura91@example.com','$2y$10$a.ikjMRnt9msi6fJ8HtRzekl2Wr/UOMlWlnxNHykyV6II1HzX7jb.','Eziln23dWK','2018-05-02 20:44:51','2018-05-02 20:44:51'),(30,'Eldred Kulas','harvey.goodwin@example.com','$2y$10$loqoonEGCuXwBNZsP3QlK.pIj/PUHQnk9EnHmmLQnhDgtoAjTFA.W','YsEedBr3UW','2018-05-02 20:44:51','2018-05-02 20:44:51'),(31,'Ms. Everette Bergstrom I','gladyce.johnston@example.net','$2y$10$ExGFpSb1t0UycxLzGm692.d6tZCu65vEGfIuqiDoBA1EdBDsOUFl2','n6eXplIGW4','2018-05-02 20:44:51','2018-05-02 20:44:51'),(32,'Adalberto O\'Kon','skozey@example.net','$2y$10$Me9dIlR2IKovja6gVpzHtu6crs0Q./7fEqPkmQFnRXERws185Y/IW','nKmd8TYiG1','2018-05-02 20:44:51','2018-05-02 20:44:51'),(33,'Miss Charity Romaguera','shannon.bode@example.net','$2y$10$j1S5dxo7WkgZZn.NSMu2K.BNmaMnFtMLikpNqJqeRPXIVzV6ZvvR6','srtbK3ncJ5','2018-05-02 20:44:51','2018-05-02 20:44:51'),(34,'Lorenz Bernhard','jamal06@example.com','$2y$10$fd6I2mpLQ.IXu2FZp5AjQujhOhEhJeYb2tQVJXupd4a4UkfiK10i.','SKnjH3VTSX','2018-05-02 20:44:51','2018-05-02 20:44:51'),(35,'Will Mante','wade.schamberger@example.net','$2y$10$0iYznVFkZ9/8V60c92DHv.Ko9KGtv9E19fIdiG6EfO.cRnT9Kb8xy','BIrnCCD8Bk','2018-05-02 20:44:51','2018-05-02 20:44:51'),(36,'Ines Schuster','skiles.haley@example.com','$2y$10$N2Jk4nFDS.2YltJTIrhe0.zz/GYRpQkhK57aark5HjJAgqu3p3W3.','q3DQioqmdH','2018-05-02 20:44:51','2018-05-02 20:44:51'),(37,'Beverly Dach','flockman@example.net','$2y$10$nK.4tQZvxcuT0Vzdm1ubW.9qp.GpnR4ViRxaTLpB5VgHQHTuAMhtm','lUR0C9kGTE','2018-05-02 20:44:51','2018-05-02 20:44:51'),(38,'Ansel Davis','willa.marvin@example.com','$2y$10$V6EJMoXYFenN/Agyu4wEMOlLiRTHRwIj5I5d9G9PDt4K4Ucv7glq.','Yzm6RzMfrl','2018-05-02 20:44:51','2018-05-02 20:44:51'),(39,'Eleanora Hickle PhD','kub.betty@example.org','$2y$10$jGOQZ0NT.4qH8sC7h8hmzeWpaMru.gTeIkM8A.p25I0wXQlB2wChy','hFBCjaseeb','2018-05-02 20:44:51','2018-05-02 20:44:51'),(40,'Julian Goldner','feil.audreanne@example.net','$2y$10$DEFtIolntNdT7dJv9/1TsOBSduKcIAEi4iDQ78u2dPeP.8MLRwJ/C','tdHcKmvqUm','2018-05-02 20:44:51','2018-05-02 20:44:51'),(41,'Glenda Ankunding','weissnat.oral@example.com','$2y$10$YH8ni61o3D650XAXr6LlHOokr.va3zlMYwd6ikRjaaihoUVFOCDzy','AwNeKnRAPs','2018-05-02 20:44:51','2018-05-02 20:44:51'),(42,'Janet Bruen','willow45@example.com','$2y$10$8AAI4SeT7xCGrS5SG3AeMOQtdlnZ61xLg2KKV/TupnTUiYncXUtAC','hvcUtLxsFx','2018-05-02 20:44:51','2018-05-02 20:44:51'),(43,'Miss Bethany Schuster IV','torphy.ella@example.org','$2y$10$cU3V8yE5iPyuax5Y6f.RMelA5/kg/YxCb0/tqXe8MwFBRKGf70fg2','tDnfFhemyu','2018-05-02 20:44:51','2018-05-02 20:44:51'),(44,'Pablo Prohaska PhD','donny94@example.org','$2y$10$sSVocqslNsJx/AFYOhcJ5.eZTUVieQRuTPL/T3k9N89M0F2aA6fja','PWZUWzlR03','2018-05-02 20:44:51','2018-05-02 20:44:51'),(45,'Jeffery Reichel','rodriguez.adah@example.net','$2y$10$xkQIxvN9V6mgY7HdoJDsE.c4ir3l98LkzGXkqxM7AoqeRYgCf/KOu','wmFTnBhySy','2018-05-02 20:44:51','2018-05-02 20:44:51'),(46,'Prof. Hilbert Rolfson DDS','vmosciski@example.com','$2y$10$JjJN9xxgN/HVTW1h0OvrceBy.VxyJuo4uAIirm3jETUhcQ262SLcm','FE1G37ot36','2018-05-02 20:44:51','2018-05-02 20:44:51'),(47,'Tad Hilpert','kkonopelski@example.com','$2y$10$e1BoXiYX0aHKSMRDiELlou7i77H26b1AXsVfhtRa0qgNKP.hAQine','004lsZkZXU','2018-05-02 20:44:51','2018-05-02 20:44:51'),(48,'Evangeline Olson DDS','tate14@example.com','$2y$10$vAlsMrRfTkNoTAn1Ms.cvOUjovTo3hregzeq8apOyr4IHQPjj.mbO','Wduzx74JE7','2018-05-02 20:44:51','2018-05-02 20:44:51'),(49,'Kiana Marvin','adelia20@example.com','$2y$10$biqY3t02tmDUTPO6UFLr0.NpDReK6RKIc9I8jE1v6uM/zpBdF/.N2','ln9W9NhQiZ','2018-05-02 20:44:51','2018-05-02 20:44:51'),(50,'Miss Elmira Lynch I','schmitt.mikayla@example.org','$2y$10$KIX2.s8WnVJNeE6mluhE4uCTM9hCQGiFI9MeiOovMoLFHFw.ofdAS','tF8ceASzyF','2018-05-02 20:44:51','2018-05-02 20:44:51'),(51,'Murl Bartell','donnie.weber@example.com','$2y$10$y6Csy0woMeQi9OID.YA/We9c4qfNigaqp71ySXze8MQghPE.dUHtu','1G2C31jZKz','2018-05-02 20:44:51','2018-05-02 20:44:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-02 22:54:49
