<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Questionnaire;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
      // $this->call(QuestionnaireTableSeeder::class);

        //disable foreign key check for this connection before running seeders
           DB::statement('SET FOREIGN_KEY_CHECKS=0;');
           Model::unguard();

           // truncate questionnaire before adding in data with ids that are set.
           questionnaire::truncate();
           $this->call(QuestionnaireTableSeeder::class);

           User::truncate();

           //re-enable foreign key check for this connection
           DB::statement('SET FOREIGN_KEY_CHECKS=1;');

           $this->call(UserTableSeeder::class);
           factory(User::class, 50)->create();

           Model::reguard();

    }
}
