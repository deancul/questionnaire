<?php

use Illuminate\Database\Seeder;

class QuestionnaireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('questionnaires')->insert([
            ['id' => 1, 'questions' => "How likely are you to answer a multiple choice questionnaire?", 'answers' => 0],

        ]);
    }
}
