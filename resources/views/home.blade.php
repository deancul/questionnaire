@extends('layouts.master')

@section('title', 'My Home Page')

@section('content')
<h1>Introduction</h1>
    <p>For this assignment I had to create a questionnaire which asks questions related to questionnaires.</p>

<h2> Questionnaire </h2>
<section>
  @if (isset ($questionnaire))

      <table class="table table-striped table-bordered">
          <thead>
          <tr>
              <td>Question</td>
              <td>Answer</td>

          </tr>
          </thead>
          <tbody>
          @foreach ($questionnaire as $questionnaire)
              <tr>
                  <td>{{ $questionnaire->questions }}</td>
                  <td>
                    {!! Form::open(['method' => 'PATCH', 'url' =>'home']) !!}
                    <div class="form-group">
                      {!! Form::label('answers', 'Select an answer:') !!}
                      {!! Form::select('answers', array('1' => 'Most likely', '2' => 'Likely', '3' => 'Do not know', '4' => 'Not likely', '5' => 'Definitely not likely'), null,['placeholder' => 'Select...']) !!}
                    </div>
                  </td>
              </tr>
          @endforeach
          </tbody>
      </table>
      <div class="form-group">
          {!! Form::submit('Submit questionnaire', ['class' => 'btn btn-primary form-control']) !!}
      </div>

  @endif


</section>

@endsection
