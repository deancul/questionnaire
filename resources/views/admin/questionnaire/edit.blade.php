<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin - edit {{ $questionnaire->title }}</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<div class="container">
    <header class="row">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <ul class="nav navbar-nav">
                    <a class="navbar-brand" href="#">Admin</a>
                    <li class="active"><a href="/">questionnaire</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <article class="row">
        <h1>Edit - {{ $questionnaire->title }}</h1>

        <!-- errors -->
        @if ($errors->any())
            <div>
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

                    <!-- form goes here -->
            {!! Form::model($questionnaire, ['method' => 'PATCH', 'url' => 'questionnaire/' . $questionnaire->id]) !!}



            <div class="form-group">
                {!! Form::label('question', 'Question:') !!}
                {!! Form::textarea('detail', null, ['class' => 'form-control']) !!}
            </div>



            <div class="form-group">
                {!! Form::submit('Update question', ['class' => 'btn btn-primary form-control']) !!}
            </div>


            {!! Form::close() !!}


    </article>
    <footer class="row">
        @include('includes.footer')
    </footer>
</div><!-- close container -->

</body>
</html>
