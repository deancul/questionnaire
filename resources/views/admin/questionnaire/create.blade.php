<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Admin - create questions</title>
    <link rel="stylesheet" href="/css/app.css" />
</head>
<body>
<div class="container">
    <header class="row">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <ul class="nav navbar-nav">
                    <a class="navbar-brand" href="#">Admin</a>
                    <li class="active"><a href="/">questionnaire</a></li>
                </ul>
            </div>
        </nav>
    </header>
    <article class="row">
        <h1>Create a new question</h1>



        <!-- form goes here -->
        {!! Form::open(['url' =>'questionnaire']) !!}



            <div class="form-group">
                {!! Form::label('questions', 'Question:') !!}
                {!! Form::textarea('questions', null, ['class' => 'form-control']) !!}
            </div>

        <!--    <div class="form-group">
                {!! Form::label('answers', 'Select an answer:') !!}
                {!! Form::select('answers', array('1' => 'Most likely', '2' => 'Likely', '3' => 'Do not know', '4' => 'Not likely', '5' => 'Definitely not likely'), null,['placeholder' => 'Select...']) !!}
            </div>
          -->

            <div class="form-group">
                {!! Form::submit('Add question', ['class' => 'btn btn-primary form-control']) !!}
            </div>

        {!! Form::close() !!}


    </article>
    <footer class="row">
        @include('includes.footer')
    </footer>
</div><!-- close container -->

</body>
</html>
