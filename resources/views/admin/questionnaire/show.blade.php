<!doctype html>
 <html>
 <head>
     <meta charset="UTF-8">
     <title>Admin - Questionnaire</title>
     <link rel="stylesheet" href="/css/app.css" />
 </head>
 <body>
 <div class="container">
     <header class="row">
         <nav class="navbar navbar-inverse navbar-fixed-top">
             <div class="container">
                 <ul class="nav navbar-nav">
                     <a class="navbar-brand" href="#">Admin</a>
                     <li class="active"><a href="/">Questions</a></li>
                 </ul>
             </div>
         </nav>
     </header>
     <article class="row">

         <div class="col-md-6">
 <h1>Questions</h1>
        </div>
        <div class="col-md-6">
        <a href="questionnaire/create" class="btn btn-lg btn-success pull-right top-buffer">Add New Question</a>
        </div>
         <section>
             @if (isset ($questionnaire))

                 <table class="table table-striped table-bordered">
                     <thead>
                     <tr>
                         <td>Question</td>
                         <td>Answer</td>
                         <td>Update</td>
                         <td>Delete</td>
                     </tr>
                     </thead>
                     <tbody>
                     @foreach ($questionnaire as $questionnaire)
                         <tr>
                             <td>{{ $questionnaire->questions }}</td>
                             <td>{{ $questionnaire->answers }}</td>

                             <td> <a href="questionnaire/{{ $questionnaire->id }}/edit" class="btn btn-warning">Update</a></td>
                             <td>
                                {!! Form::open(['method' => 'DELETE', 'route' => ['questionnaire.destroy', $questionnaire->id]]) !!}
                                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}

                              </td>
                         </tr>

                     @endforeach
                     </tbody>
                 </table>

             @else
                 <p> No questions added yet </p>
             @endif
         </section>
     </article>
     <footer class="row">
         @include('includes.footer')
     </footer>
 </div><!-- close container -->

 </body>
 </html>
