<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Questionnaire;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $questionnaire = questionnaire::all();
      return view('home', ['questionnaire' => $questionnaire]);
    }
    public function store(Request $request)
    {



        $input = $request->all();

      home::submit($input);

        return redirect('home');
    }

  }
